@extends('layouts.backend',['title' => 'Set Role'])

@section('content')
    <div class="row justify-content-center">
        <div class="col-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Set Role</h4>
                    <form class="forms-sample" method="POST" action="{{route('users.update',$user->id)}}">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="exampleInputName1">Username</label>
                            <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                                id="exampleInputName1" value="{{ $user->username }}">
                            @error('name')
                                <span class="invalid-feedback">{{ $message }}</span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="">Pilih Role</label>
                            <select 
                            class="form-control"
                            {{-- class="js-example-basic-multiple w-100"  --}}
                            name="role" >
                                <option value="">Pilih Role</option>
                                @forelse ($roles as $item)
                                <option value="{{ $item->id }}"
                                       
                                    {{$user->hasRole($item->id) ? 'selected' : ''}}
                                    >
                                    {{ $item->name }} 
                                </option>
                                @empty
                                    <option value="">Role belum dipilih</option>
                                @endforelse
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Set Role </button>
                        <button class="btn btn-light">Batal</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection