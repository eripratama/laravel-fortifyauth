@extends('layouts.backend',['title' => 'List User'])

@section('content')
<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">List User</h4>
                
                <table class="table">
                    <tr>
                        <thead>
                            <th>No</th>
                            <th>Username</th>
                            <th>Opsi</th>
                        </thead>
                        @forelse ($users as $item)
                            <tbody>
                                <td>{{ ++$i }}</td>
                                <td>{{ $item->name }}</td>
                                <td>
                                    <form onsubmit="return confirm('Yakin hapus data..?');"
                                        action="{{ route('role-permission.destroy', $item->id) }}" method="POST">
                                        <a href="{{ route('users.edit', $item->id) }}"
                                            class="btn btn-warning btn-sm">Set Role</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger btn-sm">Hapus</button>
                                    </form>
                                </td>
                            @empty
                                <td>Data Kosong</td>
                            </tbody>
                            <form method="POST" id="delete-form" class="d-none"
                                action="{{ route('role-permission.destroy', $item->id) }}">
                                @csrf
                                @method('DELETE')
                            </form>
                        @endforelse
                    </tr>
                </table>
                {{ $users->links() }}
            </div>
        </div>
    </div>
</div>
@endsection