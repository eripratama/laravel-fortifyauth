<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectsController extends Controller
{
    public function index()
    {
        if (Auth::user()->hasRole('Admin')) {
            return redirect()->route('admin.dashboard.index');
        } else {
            return redirect()->route('welcome');
        }
    }
}
