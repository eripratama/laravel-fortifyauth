<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::paginate(5);
        return view('backend.user.index',[
            'users' => $users
        ])->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        //$userRole = $user->roles->all();

        return view('backend.user.setRole',compact('user','roles'));
    }

    public function update(Request $request,$id)
    {
        $user = User::find($id);
        $user->update([
            $user->assignRole($request->role),
        ]);

        return redirect()->route('users.index');
    }
}
