<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        //$user = Auth::user();
        return view('backend.profile.index',[
            'user' => $request->user()
        ]);
    }

    public function update(Request $request,User $user)
    {
        
        $user = User::findOrFail($user->id);
        $user->name = $request->name;
        $user->email = $request->email;

        if ($request->file('image')) {
            Storage::disk('local')->delete('public/profileImage/'.basename($user->image));
            $image = $request->file('image');
            $image->storeAs('public/profileImage',$image->hashName());
            $user->image = $image->hashName();
        }
        //$user->save();
       
        return dd($user);
        //return redirect()->route('admin.profile.index');
    }
}
