<?php

use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\PermissionController;
use App\Http\Controllers\Admin\ProfileController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;

Route::prefix('admin')->middleware(['auth','verified'])->group(function () {
    Route::get('/dashboard',[DashboardController::class,'index'])->name('admin.dashboard.index');       
    Route::get('/profile/{id}',[ProfileController::class,'index'])->name('profile.index');
    Route::put('/profile/{id}',[ProfileController::class,'update'])->name('profile.update');
    
    Route::resource('/role-permission', RoleController::class);
    Route::resource('/permission', PermissionController::class);
    Route::resource('/users', UserController::class);               
});